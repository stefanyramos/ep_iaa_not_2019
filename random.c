#include <stdio.h>
#include <stdlib.h>

#define MAX 2147483647 //no int overflow

void concat(char *a, char *b){
	while(* ++a);
  	while(*(a++) = (*b++));
}
int prng(){
	float i = rand()*rand();
	return (rand()%2)?(int)i%MAX:(int)-i%MAX;
}
void fulf(char* name, int* size){
	concat(name, ".txt");
	FILE *f = fopen(name, "w");
	fprintf(f, "%d\n", *size);
	while((*size)--) fprintf(f, "%d\n", prng());
	fclose(f);
}
int pot(int b, int e){
	int n=1;
	while (e--) n*=b;
	return n;
}
void array(int i, int j){
	char name[100];
	int size = 1000*pot(10, i);
	if(size<1000000)sprintf(name, "%dK_Array_%d", size/1000, j+1);
	else sprintf(name, "%dM_Array_%d", size/1000000, j+1);
	fulf(name, &size);
}
int main(){
	int i, j;
	for(i=0; i<4; i++)
		for(j=0; j<250; j++)
			array(i, j);
	return 0;
}